package main

import (
	"reflect"
	"testing"
)

func TestDNSMessageToBytes(t *testing.T) {
	dnsHeader := &DNSHeader{
		PacketID:              1234,
		QueryRespIndicator:    1,
		OpCode:                0,
		AuthoritativeAns:      1,
		Truncation:            0,
		RecursionDesired:      1,
		RecursionAvailable:    0,
		Reserved:              0,
		ResponseCode:          11,
		QuestionCount:         2,
		AnsRecordCount:        2,
		AuthorityRecordCount:  0,
		AdditionalRecordCount: 0,
	}
	dnsQuestion1 := &DNSQuestion{
		Name:  convertToDNSName("codecrafters.io"),
		Type:  1,
		Class: 1,
	}

	dnsQuestion2 := &DNSQuestion{
		Name:  []byte{1, 0b01100001, 0b11000000, 0b00001100},
		Type:  1,
		Class: 1,
	}

	dnsMessage := &DNSMessage{
		Header: dnsHeader,
		Questions: []*DNSQuestion{
			dnsQuestion1,
			dnsQuestion2,
		},
	}

	dnsMessageBytes := dnsMessage.Bytes()
	headerBytes := []byte{4, 210, 133, 11, 0, 12, 0, 12, 0, 0, 0, 0}
	questionBytes := []byte{12, 99, 111, 100, 101, 99, 114, 97, 102, 116, 101, 114, 115, 2, 105, 111, 0, 0, 1, 0, 1}
	expectedBytes := append(append(headerBytes, questionBytes...), questionBytes...)

	if !reflect.DeepEqual(dnsMessageBytes, expectedBytes) {
		t.Errorf("expected \n%v\n found \n%v\n", expectedBytes, dnsMessageBytes)
	}
}

// func TestBytesToDNSMessageToUnCompressedBytes(t *testing.T) {
// 	compressedBytes := []byte{4, 210, 133, 11, 0, 2, 0, 2, 0, 0, 0, 0, 12, 99, 111, 100, 101, 99, 114, 97, 102, 116, 101, 114, 115, 2, 105, 111, 0, 0, 1, 0, 1, 1, 97, 192, 12, 0, 1, 0, 1}
// 	_, dnsMessage, _ := BytesToDNSMessage(compressedBytes)
// 	dnsMessageBytes := dnsMessage.Bytes()
// 	headerBytes := []byte{4, 210, 133, 11, 0, 2, 0, 2, 0, 0, 0, 0}
// 	questionBytes := []byte{12, 99, 111, 100, 101, 99, 114, 97, 102, 116, 101, 114, 115, 2, 105, 111, 0, 0, 1, 0, 1}
// 	expectedBytes := append(append(headerBytes, questionBytes...), questionBytes...)
//
// 	if !reflect.DeepEqual(dnsMessageBytes, expectedBytes) {
// 		t.Errorf("expected \n%v\n found \n%v\n", expectedBytes, dnsMessageBytes)
// 	}
// }

func TestBytesToDNSMessage_CompressedLabels(t *testing.T) {
	// Compressed DNS Message (Manually Constructed)
	compressedMessage := []byte{
		0x12, 0x34, // Packet ID
		0x81, 0x80, // Flags: Standard query, Response
		0x00, 0x02, // Question Count: 2
		0x00, 0x02, // Answer Count: 2
		0x00, 0x00, // Authority Record Count: 0
		0x00, 0x00, // Additional Record Count: 0

		// Question 1:
		0x03, 0x61, 0x62, 0x63, // Label: abc
		0x07, 0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, // Label: example
		0x03, 0x63, 0x6f, 0x6d, // Label: com
		0x00,       // End of label
		0x00, 0x01, // Type: A
		0x00, 0x01, // Class: IN

		// Question 2 (Compressed):
		0x03, 0x64, 0x65, 0x66, // Label: def
		0xC0, 0x0C, // Pointer to 'example.com'
		0x00, 0x01, // Type: A
		0x00, 0x01, // Class: IN

		// ... (Answer Section - Similar Structure)
	}

	// Expected Output
	expectedQuestions := []*DNSQuestion{
		{Name: []byte("abc.example.com."), Type: 1, Class: 1},
		{Name: []byte("def.example.com."), Type: 1, Class: 1},
	}

	// Test Function
	_, parsedMessage, err := BytesToDNSMessage(compressedMessage)
	if err != nil {
		t.Errorf("Error parsing DNS message: %v", err)
	}

	if len(parsedMessage.Questions) != len(expectedQuestions) {
		t.Errorf("Incorrect question count. Expected: %d, Got: %d", len(expectedQuestions), len(parsedMessage.Questions))
	}

	for i := range expectedQuestions {
		if string(parsedMessage.Questions[i].Name) != string(expectedQuestions[i].Name) {
			t.Errorf("Question %d mismatch. Expected: %s, Got: %s", i+1, expectedQuestions[i].Name, parsedMessage.Questions[i].Name)
		}
		// ... Similarly compare Type and Class
	}
}
