package main

import (
	"reflect"
	"testing"
)

func TestQuestionsToBytes(t *testing.T) {
	dnsQuestion := &DNSQuestion{
		Name:  convertToDNSName("codecrafters.io"),
		Type:  1,
		Class: 1,
	}

	questionBytes := dnsQuestion.Bytes()
	expectedBytes := []byte{12, 99, 111, 100, 101, 99, 114, 97, 102, 116, 101, 114, 115, 2, 105, 111, 0, 0, 1, 0, 1}

	if !reflect.DeepEqual(expectedBytes, questionBytes) {
		t.Errorf("Expected %v Found %v", expectedBytes, questionBytes)
	}
}

func TestBytesToQuestion(t *testing.T) {
	questionBytes := []byte{12, 99, 111, 100, 101, 99, 114, 97, 102, 116, 101, 114, 115, 2, 105, 111, 0, 0, 1, 0, 1}
	_, dnsQuestion, err := BytesToDNSQuestion(questionBytes, 0)
	expectedDNSQuestion := DNSQuestion{
		Name:  convertToDNSName("codecrafters.io"),
		Type:  1,
		Class: 1,
	}

	if !reflect.DeepEqual(*dnsQuestion, expectedDNSQuestion) {
		t.Errorf("Expected %v Found %v err %v", expectedDNSQuestion, dnsQuestion, err)
	}
}
