package main

import (
	"flag"
	"fmt"
	"net"
)

var resolver string

func sendBytesToDNSServer(resolver string, dnsMessageQuery DNSMessage) (*DNSMessage, error) {
	conn, err := net.Dial("udp", resolver)

	if err != nil {
		fmt.Println("Unable to stablish connection with DNS server")
	}

	defer conn.Close()

	conn.Write(dnsMessageQuery.Bytes())
	buff := make([]byte, 512)
	size, err := conn.Read(buff)

	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return nil, err
	}

	_, dnsMessage, err := BytesToDNSMessage(buff[:size])
	if err != nil {
		return nil, err
	}
	return dnsMessage, nil
}

func fanOutDNSQuestions(dnsMessage *DNSMessage) []DNSMessage {
	dnsMessages := []DNSMessage{}
	fmt.Println("Fanning out DNS Questions", dnsMessage.Header.QuestionCount)
	for i := 0; i < int(dnsMessage.Header.QuestionCount); i++ {
    header := *dnsMessage.Header
		currDnsMessage := DNSMessage{
			Header: &header,
			Questions: []*DNSQuestion{
				dnsMessage.Questions[i],
			},
		}

		currDnsMessage.Header.QuestionCount = 1
		currDnsMessage.Header.AnsRecordCount = 1
		dnsMessages = append(dnsMessages, currDnsMessage)
	}
	fmt.Println("Fanning out DNS Questions len", len(dnsMessages))
	fmt.Println("After Fanning out DNS Questions", dnsMessage.Header.QuestionCount)
	return dnsMessages
}

func fanInDNSAnswers(dnsMessages []DNSMessage) DNSMessage {
	dnsMessage := DNSMessage{
		Header:    &(*dnsMessages[0].Header),
		Answer:    []*DNSAnswer{},
	}

	ansCount := 0

	for i := 0; i < len(dnsMessages); i++ {
		for j := 0; j < int(dnsMessages[i].Header.AnsRecordCount); j++ {
			dnsMessage.Answer = append(dnsMessage.Answer, &(*dnsMessages[i].Answer[j]))
			ansCount += 1
			fmt.Println("Fanning In DNS Answer", dnsMessages[i].Answer[j].Data.a)
		}
	}
	fmt.Println("DNS Query Answer Count", ansCount)
	dnsMessage.Header.AnsRecordCount = uint16(ansCount)
	return dnsMessage
}

func sendDNSQueries(resolver string, dnsmessage DNSMessage) DNSMessage {
	dnsMessages := fanOutDNSQuestions(&dnsmessage)

	dnsMessageResponses := []DNSMessage{}

	for i := 0; i < len(dnsMessages); i++ {
		dnsMessage, err := sendBytesToDNSServer(resolver, dnsMessages[i])

		if err != nil {
			fmt.Println("Error while proxying DNS queries", err)
		}

		dnsMessageResponses = append(dnsMessageResponses, *dnsMessage)
	}

	dnsMessageResponse := fanInDNSAnswers(dnsMessageResponses)
  dnsMessageResponse.Questions = dnsmessage.Questions
	dnsMessageResponse.Header.QuestionCount = dnsmessage.Header.QuestionCount

	return dnsMessageResponse
}

func main() {
	flag.StringVar(&resolver, "resolver", "", "")
	flag.Parse()

	if resolver != "" {
		fmt.Printf("Sending DNS queries to %s\n", resolver)
	}

	fmt.Println("Logs from your program will appear here!")

	udpAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:2053")
	if err != nil {
		fmt.Println("Failed to resolve UDP address:", err)
		return
	}

	udpConn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		fmt.Println("Failed to bind to address:", err)
		return
	}
	defer udpConn.Close()

	buf := make([]byte, 512)

	for {
		size, source, err := udpConn.ReadFromUDP(buf)
		if err != nil {
			fmt.Println("Error receiving data:", err)
			break
		}

		recievedBuffer := buf[:size]

		_, recievedDNSQuery, err := BytesToDNSMessage(recievedBuffer)

		if err != nil {
			fmt.Println("Failed to Parse DNSMessage:", err)
			return
		}
		receivedData := string(buf[:size])
		fmt.Printf("Received %d bytes from %s: %s\n", size, source, receivedData)

		dnsResponse := sendDNSQueries(resolver, *recievedDNSQuery)

		fmt.Println("Responding with PacketID:", dnsResponse.Header.PacketID)
		fmt.Println("Responding with Questions:", dnsResponse.Questions)
		fmt.Println("Responding with Answers:", dnsResponse.Answer)

		_, err = udpConn.WriteToUDP(dnsResponse.Bytes(), source)
		if err != nil {
			fmt.Println("Failed to send response:", err)
		}
	}
}
