package main

import "encoding/binary"

type IPV4Address struct {
	a, b, c, d uint8
}

type DNSAnswer struct {
	Name   []DomainLabel
	Type   uint16
	Class  uint16
	TTL    uint32
	Length uint16
	Data   IPV4Address
}

func (answer *DNSAnswer) Bytes() []byte {
	buff := []byte{}
	for _, domainLabel := range answer.Name {
		b, err := domainLabel.Binary()
		if err != nil {
			return nil
		}
		buff = append(buff, b...)
	}

	buff = append(buff, 0)
	buff = binary.BigEndian.AppendUint16(buff, answer.Type)
	buff = binary.BigEndian.AppendUint16(buff, answer.Class)
	buff = binary.BigEndian.AppendUint32(buff, answer.TTL)
	buff = binary.BigEndian.AppendUint16(buff, answer.Length)
	buff = append(buff, answer.Data.a, answer.Data.b, answer.Data.c, answer.Data.d)
	return buff
}

func BytesToAnswer(dnsMessageBytes []byte, startPos uint16) (nextAnswerIndex int, dnsAnswer *DNSAnswer, err error) {
	dnsAnswer = &DNSAnswer{}
	domainLabels := []DomainLabel{}
	nextAnswerIndex, err = ParseDomainLabels(int(startPos), dnsMessageBytes, &domainLabels)
	dnsAnswer.Name = domainLabels
	dnsAnswer.Type = 1
	dnsAnswer.Class = 1
	dnsAnswer.TTL = binary.BigEndian.Uint32(dnsMessageBytes[nextAnswerIndex : nextAnswerIndex+4])
	dnsAnswer.Length = binary.BigEndian.Uint16(dnsMessageBytes[nextAnswerIndex+4 : nextAnswerIndex+6])
	dnsAnswer.Data = IPV4Address{
		dnsMessageBytes[nextAnswerIndex+6],
		dnsMessageBytes[nextAnswerIndex+7],
		dnsMessageBytes[nextAnswerIndex+8],
		dnsMessageBytes[nextAnswerIndex+9],
	}
	nextAnswerIndex = nextAnswerIndex + 10
	return nextAnswerIndex, dnsAnswer, err
}
