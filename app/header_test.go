package main

import (
	"bytes"
	"fmt"
	"testing"
)

func TestDNSHeaderToBytes(t *testing.T) {
	dnsHeader := &DNSHeader{
		PacketID:              1234,
		QueryRespIndicator:    1,
		OpCode:                0,
		AuthoritativeAns:      1,
		Truncation:            0,
		RecursionDesired:      1,
		RecursionAvailable:    0,
		Reserved:              0,
		ResponseCode:          11,
		QuestionCount:         12,
		AnsRecordCount:        12,
		AuthorityRecordCount:  0,
		AdditionalRecordCount: 0,
	}

	headerBytes := dnsHeader.Bytes()

	expectedBytes := []byte{4, 210, 133, 11, 0, 12, 0, 12, 0, 0, 0, 0}

	if !bytes.Equal(headerBytes, expectedBytes) {
		t.Error(fmt.Sprintf("Expected %v Found %v", expectedBytes, headerBytes))
	}
}

func TestDNSBytesToHeader(t *testing.T) {
	headerBytes := []byte{4, 210, 133, 11, 0, 12, 0, 12, 0, 0, 0, 0}
  _, dnsHeader := BytesToDNSHeader(headerBytes)

	expectedDNSHeader := DNSHeader{
		PacketID:              1234,
		QueryRespIndicator:    1,
		OpCode:                0,
		AuthoritativeAns:      1,
		Truncation:            0,
		RecursionDesired:      1,
		RecursionAvailable:    0,
		Reserved:              0,
		ResponseCode:          11,
		QuestionCount:         12,
		AnsRecordCount:        12,
		AuthorityRecordCount:  0,
		AdditionalRecordCount: 0,
	}
	if *dnsHeader != expectedDNSHeader {
		t.Error(fmt.Sprintf("Exepected %v Found %v", expectedDNSHeader, dnsHeader))
	}
}
