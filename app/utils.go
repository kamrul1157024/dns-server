package main

import "strings"

func convertToDNSName(name string) []byte {
	buff := []byte{}
	labels := strings.Split(name, ".")
	for j := range labels {
		buff = append(buff, uint8(len(labels[j])))
		buff = append(buff, []byte(labels[j])...)
	}

  buff = append(buff, 0x00)

	return buff
}


