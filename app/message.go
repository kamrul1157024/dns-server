package main

type DNSMessage struct {
	Header    *DNSHeader
	Questions []*DNSQuestion
	Answer    []*DNSAnswer
}

func (message *DNSMessage) Bytes() []byte {
	buff := []byte{}
	buff = append(buff, message.Header.Bytes()...)
	for _, question := range message.Questions {
		buff = append(buff, question.Bytes()...)
	}

	for _, answer := range message.Answer {
		buff = append(buff, answer.Bytes()...)
	}

	return buff
}

func BytesToDNSMessage(messageBytes []byte) (uint16, *DNSMessage, error) {
	dnsMessage := &DNSMessage{
		Questions: []*DNSQuestion{},
		Answer: []*DNSAnswer{},
	}
	currPos := uint16(0)
	headerSize, dnsHeader := BytesToDNSHeader(messageBytes)
	dnsMessage.Header = dnsHeader
	currPos += headerSize
	for i := 0; i < int(dnsHeader.QuestionCount); i++ {
		nextPos, dnsQuestion, err := BytesToDNSQuestion(messageBytes, currPos)
		currPos = nextPos
		dnsMessage.Questions = append(dnsMessage.Questions, dnsQuestion)
		if err != nil {
			return 0, nil, err
		}
	}

  for i:=0 ;  i < int(dnsHeader.AnsRecordCount); i++ {
    nextPos, dnsAnswer, err := BytesToAnswer(messageBytes, currPos)
    dnsMessage.Answer = append(dnsMessage.Answer, dnsAnswer)
    currPos = uint16(nextPos)
    if err != nil {
			return 0, nil, err
		}
  }
	return currPos, dnsMessage, nil
}
